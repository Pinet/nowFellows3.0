class UsersController < ApplicationController

  def profil
  end

  def chat
    session[:conversations] ||= []

    @users = User.all.where(email: "marc@hotmail.fr")
    @conversations = Conversation.where(recipient: 3)
  end

  def chatAdmin
    session[:conversations] ||= []

    @users = User.all.where.not(id: current_user)
    @conversations = Conversation.includes(:recipient, :messages)
                                 .find(session[:conversations])
  end



end
